---
Spike Report
---

Core Spike 4
============

Introduction
------------

Currently C++ is considered the industry standard for Software
Programming and is also used in Unreal Engine 4 so to get the most of
the engine it is important to at least have a decent grasp of C++. An
easy way to learn C++ is to make a small guessing game in Visual Studio.

Bitbucket Link: <https://bitbucket.org/bSalmon842/2018_spike4>

Goals
-----

1.  A simple numeric guessing game on the Command Line/Console, written
    in C++

    a.  A random number between 0-99 (inclusive) is selected by the
        program to be guessed by the user

    b.  The user is prompted to enter a number until they do so

        i.  Ignoring non-numeric input, and prompting the user to try
            again

    c.  Once a number has been given, it is checked for correctness

        i.  If correct, the game ends, and the user is told how many
            guesses they made

    d.  If not correct, the user is told whether the number they are
        seeking is smaller or larger than the input number

Personnel
---------

  ------------------------- ------------------
  Primary -- Brock Salmon   Secondary -- N/A
  ------------------------- ------------------

Technologies, Tools, and Resources used
---------------------------------------

-   <http://www.cplusplus.com/reference/random/mt19937/>

-	<https://stackoverflow.com/questions/22923551/generating-number-0-1-using-mersenne-twister-c>

-	<https://blackhole12.com/blog/c-to-c-tutorial-part-1-basics-of-syntax/>

Tasks undertaken
----------------

1.  Create a new C++ Project in Visual Studio

2.  Create a new file called 'main.cpp' (NOTE: main.cpp must be present
    in the project as this is the filename used as an entry point for
    Visual C++).

3.  To setup the random number generator a random_device was first created
	to get the seed for the Mersenne Twister Generator, then using a
	Uniform Integer Distribution from 0 to 99 a number can be generated.
	For this step \<random\> is needed to be included.

4.  Make a bool variable to store if the player has guessed the correct
    number (False by default)

5.  Make an int variable to store the player's input for that turn.

6.  Include \<iostream\> at the top of the file so that the file
    recognizes the cout and cin functions

7.  In a while loop fill out how a turn is processed, prompt the user
    for input, process the input and output an error if necessary,
    otherwise continue and tell if the guess was too high or low.
	
8.	Make a variable to hold the number of guesses the player has made.
	Iterate this variable at the start of every guess and reset it to 0
	when the game is reset.

9.  If the user gets the number correct exit the while loop into
    another, which prompts the player if they want to play again.
	
10.	Create a test class with comments to explain the basic differences
	between C# and C++

What we found out
-----------------

In this Spike we learned how to use basic C++ to create a small terminal
game in Visual Studio while learning how some basic statements in C++
work. Using C++'s stdout (Standard Output) text and other values can
be displayed in a Windows Command Prompt, and also be input into the same
window. This teaches how to make use of the C Standard Library and the
variety of tools a programmer can bring into a program, with the trade-off
being that the Standard Library functions and classes can bloat the program
size and also be slower than a custom made solution, however for a 
beginner it allows access to many conveniences. 

This Spike also teaches a lot about scope and loops, and how to handle different
varaibles that might be needed between loops, only accessible to one loop, etc.

In the program is also a heavily commented class describing the differences in
syntax between C++ and C#, which some people may have more experience with.
Learning how to compare syntax is a vital tool for a versatile programmer so that
they can pick up almost any documented programming language and learn quickly to
become effectives with it by replicating past patterns in the new syntax.

Recommendations
---------------

It can be beneficial to have references open if the user is new to C++,
such as <http://www.cplusplus.com/> . From there the user can look for
better ways to structure the Random Number Generator or for accepting
inputs from the player.
