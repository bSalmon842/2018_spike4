/*
Project: SGD240 2018 Spike 4
File: main.cpp
Author: Brock Salmon
Notice: (C) Copyright 2018 by Brock Salmon. All Rights Reserved.
*/

#include <iostream>
#include <random>

// Comparing C# and C++ Syntax
//#include <iostream> C++ uses Includes to bring in different libraries instead of "Using"
// C++ code can exist in 2 different types of files, .cpp files and .h (/.hpp) files
class SyntaxComparison
{
	// Most type keywords are the same
	float *pointer = 0; // Pointers are used to point to a memory address that contains a value that is stored on the heap
	
	SyntaxComparison();
	~SyntaxComparison();
	
	// Pointers can be passed into functions, this allows you to change the value being
	// pointed to in the function, unlike Copy passing, which doesn't change the value outside 
	// of the scope of the function, this is similar to the 'ref' keyword in C#
	void funcPrototype(float *pointerPass); // Function Prototypes are allowed in C++
	
	void function()
	{
		// In C++ Namespaces are accessed using :: instead a . like in C#
		std::cout << "Hello World" << std::endl;
		
		// However class/struct members are still accessed using a . or in the case of 
		// pointers, a ->
		this->funcPrototype(this->pointer);
	}
};

int main(int argCount, char *args[])
{
	// Allow the player to replay the game without restarting the program, loop will return to exit function when exiting
	while (true)
	{
		// Generate a number between 0 & 99 and assign it to variable answer
		std::random_device device; // Used to obtain seed for the Random Number Engine
		std::mt19937 generator(device());
		std::uniform_int_distribution<int> dist(0, 99);
		int answer = dist(generator);
		
		bool correct = false; // Used for game loop
		int input; // Used to store the player's answer
		int guesses = 0; // User's current guess count
		
		std::cout << "\n\n\n\nGuess a number between 0 - 99!" << std::endl;
		
		while (!correct)
		{
			guesses++;
			
			std::cout << "\n\nEnter a Number to Guess: ";
			std::cin >> input;
			
			if (input == answer) // Correct Answer
			{
				std::cout << "\nCorrect! You got the answer in " << guesses << " guesses!" << std::endl;
				correct = true;
			}
			else if (input < 0 || input > 99) // Undefined Answer
			{
				std::cout << "\nOut of Range Answer! Make sure your answer is between 0 & 99" << std::endl;
				std::cin.clear();
				std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			}
			else if (input < answer) // input is less than answer
			{
				std::cout << "\nThe answer is greater than " << input << std::endl;
			}
			else if (input > answer) // input is greater than answer
			{
				std::cout << "\nThe answer is less than " << input << std::endl;
			}
		}
		
		char playAgain;
		
		std::cout << "Do you want to play again? (y/n)" << std::endl;
		
		while (true) // No need to use n as an argument as it would exit the program otherwise
		{
			std::cin >> playAgain;
			if (playAgain == 'y')
			{
				guesses = 0; // Reset Guess Count
				break; // Do Nothing as it will loop anyway
			}
			else if (playAgain == 'n')
			{
				return 0; // Exit the program
			}
			else // If the user input something other than y or n
			{
				std::cout << "Undefined, please input y or n" << std::endl;
			}
		}
	}
	
	return 0;
}